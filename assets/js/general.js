/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function insertDataQuery(_url, _postData, callbackFunction, type, preprocess)
{
    if (type == null)
    {
        type = "html";
    }
    if (preprocess == null)
    {

    }
    $.ajax({type: "POST", url: _url, data: _postData,async: false,beforeSend: preprocess, dataType: type,
        success: function (_returnData)
        {
            parseResult(callbackFunction, _returnData);
        },
        error: function (error)
        {

            console.log(error.responseText);
        },
        beforeSend: function(){
           
        },
        complete: function(){
           
        }
    });
}

function generalQuery(_url, _action, _postData, callbackFunction, preprocess, type, _headerinfo)
{
    if (type == null)
    {
        type = "html";
    }
    if (preprocess == null)
    {

    }
    $.ajax({type: _action, url: _url, data: _postData, beforeSend: preprocess, dataType: type,
        headers: {"API-Key": _headerinfo},
        success: function (_returnData)
        {
            parseResult(callbackFunction, _returnData);
        },
        error: function (error)
        {
            console.log(error.responseText);
        },
        beforeSend: function(){
           
        },
        complete: function(){
           
        }
    });

}



function parseResult(callbackFunction, _returnData)
{
    callbackFunction(_returnData);
}

function reloadPagePart(base_url,element, controller, action, paramerters)
{
    var url = base_url + controller + '/' + action + '/';
    var setLoadedData = function (data)
    {
        $(element).replaceWith(data);
        $('body').animate({scrollTop: 0}, 800);
    };
    generalQuery(url, 'POST', paramerters, setLoadedData);
}

function reloadPagePart1(base_url,element,paramerters)
{
    var url = base_url;
    var setLoadedData = function (data)
    {
        $(element).replaceWith(data);
        $('body').animate({scrollTop: 0}, 800);
    };
    generalQuery(url, 'POST', paramerters, setLoadedData);
}

function showPopupNotification(_title, _text, _image, _stickey, _time)
{

    $.gritter.add({
        // (string | mandatory) the heading of the notification
        title: _title,
        // (string | mandatory) the text inside the notification
        text: _text,
        // (string | optional) the image to display on the left
        image: 'https://autotouch.net/server/static/img/icon-validated.png',
        // (bool | optional) if you want it to fade out on its own or just sit there
        sticky: false,
        // (int | optional) the time you want it to be alive for before fading out
        class_name: _title,
    });

    return false;
}
