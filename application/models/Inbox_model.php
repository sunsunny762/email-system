<?php

class Inbox_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getMessages($id) {
        $this->db->select('u.*,c.*');
        $this->db->from(TBL_COMPOSE. ' c');
        $this->db->join(TBL_USERS.' u','c.user_id=u.user_id','left');
        $this->db->where('c.to', $id);
        $this->db->where('c.status',1);
        $result = $this->db->get()->result_array();
        if (count($result) > 0) {
            return $result;
        }
        return array();
    }
    
    function getSentMessages($id) {
        $this->db->select('u.*,c.*');
        $this->db->from(TBL_COMPOSE. ' c');
        $this->db->join(TBL_USERS.' u','c.user_id=u.user_id','left');
        $this->db->where('c.user_id', $id);
        $result = $this->db->get()->result_array();
        if (count($result) > 0) {
            return $result;
        }
        return array();
    }
    
    function getEmailDetail($email_id) {
        $this->db->select('u.*,c.*');
        $this->db->from(TBL_COMPOSE. ' c');
        $this->db->join(TBL_USERS.' u','c.user_id=u.user_id','left');
        $this->db->where('c.c_id', $email_id);
        $result = $this->db->get()->result_array();
        if (count($result) > 0) {
            return $result[0];
        }
        return array();
    }
    
    function getReplyEmailDetail($email_id) {
        $this->db->select('u.*,cr.*');
        $this->db->from(TBL_COMPOSE. ' c');
        $this->db->join(TBL_CONVERSATION_REPLY.' cr','c.c_id=cr.c_id','left');
        $this->db->join(TBL_USERS.' u','cr.user_id=u.user_id','left');
        $this->db->where('cr.c_id', $email_id);
        $result = $this->db->get()->result_array();
        if (count($result) > 0) {
            return $result;
        }
        return array();
    }
    
    function getOtherUsers($id) {
        $this->db->select('*');
        $this->db->from(TBL_USERS);
        $this->db->where_not_in('user_id',$id);
        $result = $this->db->get()->result_array();
        if (count($result) > 0) {
            return $result;
        }
        return array();
    }
    
    function composeEmail($data) {
        $this->db->insert(TBL_COMPOSE, $data);
        return $this->db->insert_id();
    }
    
    function replyEmail($data) {
        $this->db->insert(TBL_CONVERSATION_REPLY, $data);
        return $this->db->insert_id();
    }
    
    function deleteEmail($id) {
        $data = array(
          'status' => 0  
        );
        $this->db->where('c_id', $id);
        $this->db->update(TBL_COMPOSE,$data);
        return true;
    }
}

?>