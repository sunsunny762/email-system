<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Inbox extends CI_Controller {

    public function __Construct() {

        parent::__Construct();
        $this->load->model('inbox_model');
    }

    public function index($user_id = "") {
        $data = array();
        if(isset($_GET['user_id']) && $_GET['user_id'] != ''){
            $user_id = $_GET['user_id'];
        }
        if (isset($user_id) && $user_id != '') {
            $data['data'] = $this->inbox_model->getMessages($user_id);
            $data['users'] = $this->inbox_model->getOtherUsers($user_id);
            $data['user_id'] = $user_id;
            if (isset($_POST['type']) && $_POST['type'] == 'loadPart') {
                $this->load->view('inbox', $data);
            } else {
                $this->load->view('templates/header');
                $this->load->view('inbox', $data);
            }
        }else{
            $this->load->view('templates/header');
                $this->load->view('not-found');
        }
    }

    public function sent($user_id = "") {
        $data = array();
        if (isset($user_id) && $user_id != '') {
            $data['data'] = $this->inbox_model->getSentMessages($user_id);
            $data['users'] = $this->inbox_model->getOtherUsers($user_id);
            $data['user_id'] = $user_id;
            if (isset($_POST['type']) && $_POST['type'] == 'loadPart') {
                $this->load->view('sent', $data);
            } else {
                $this->load->view('templates/header');
                $this->load->view('sent', $data);
            }
        }else{
            $this->load->view('templates/header');
            $this->load->view('not-found');
        }
    }

    public function email($email_id = "") {
        $data = array();
        if (isset($email_id) && $email_id != '') {
            $data['data'] = $this->inbox_model->getEmailDetail($email_id);
            $data['reply_data'] = $this->inbox_model->getReplyEmailDetail($email_id);
            if (isset($data['data']['user_id']) && $data['data']['user_id'] != '') {
                $data['users'] = $this->inbox_model->getOtherUsers($data['data']['user_id']);
                $data['user_id'] = $data['data']['to'];
                $data['email_id'] = $email_id;
                if (isset($_POST['type']) && $_POST['type'] == 'loadPart') {
                    $this->load->view('email', $data);
                } else {
                    $this->load->view('templates/header');
                    $this->load->view('email', $data);
                }
            }else{
                $this->load->view('templates/header');
                $this->load->view('not-found');
            }
        }else{
            $this->load->view('templates/header');
            $this->load->view('not-found');
        }
    }

    public function compose_email() {
        if (isset($_POST['user_id']) && isset($_POST['c_to'])) {
            $data = array(
                'user_id' => $_POST['user_id'],
                'to' => $_POST['c_to'],
                'subject' => $_POST['c_subject'],
                'message' => $_POST['c_message'],
                'time' => date('Y-m-d H:i:s')
            );
            $compose_id = $this->inbox_model->composeEmail($data);
            echo json_encode(array('result' => 'success', 'id' => $compose_id));
            die;
        }
    }

    public function reply_email() {
        if (isset($_POST['user_id']) && isset($_POST['c_id'])) {
            $data = array(
                'user_id' => $_POST['user_id'],
                'c_id' => $_POST['c_id'],
                'subject' => $_POST['r_subject'],
                'message' => $_POST['r_message'],
                'time' => date('Y-m-d H:i:s')
            );
            $reply_id = $this->inbox_model->replyEmail($data);
            echo json_encode(array('result' => 'success', 'id' => $reply_id));
            die;
        }
    }

    public function delete_email($id) {
        if (isset($id)) {
            $this->inbox_model->deleteEmail($id);
            echo json_encode(array('result' => 'success'));
            die;
        }
        echo json_encode(array('result' => 'fail'));
        die;
    }
}
