<div class="container" id="email_container">
     <div class="row">
        <div class="col-sm-2 col-md-2">
            <a href="<?php echo base_url('inbox/index/'.$user_id);?>" class="btn btn-success btn-sm btn-block" role="button">Back to Inbox</a>
        </div>
         <h2>Email Detail</h2>
     </div>
    <div class="row">
        <div class="col-sm-7">
            <hr/>
            <div class="review-block">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="review-block-name">username : <?php echo $data['username'];?></div>
                        <div class="review-block-date"><?php echo date('d/m/Y H:i:s',strtotime($data['time']));?></div>
                    </div>
                    <div class="col-sm-9">
                        <div class="review-block-title">Subject :<?php echo $data['subject'];?></div>
                        <div class="review-block-description">Message :<?php echo $data['message'];?></div>
                    </div>
                </div>
                <hr/>
            </div>
        </div>
        <?php if(isset($reply_data) && count($reply_data) > 0){ ?> 
        <?php foreach ($reply_data as $value){ ?>
        <div class="col-sm-7">
            <hr/>
            <div class="review-block">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="review-block-name">Type : Reply</div>
                        <div class="review-block-name">User Name : <?php echo $value['username'];?></div>
                    </div>
                    <div class="col-sm-9">
                        <div class="review-block-title">Subject :<?php echo $value['subject'];?></div>
                        <div class="review-block-description">Message :<?php echo $value['message'];?></div>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="review-block-date"><?php echo date('d/m/Y H:i:s',strtotime($value['time']));?></div>
                </div>
            </div>
        </div>
        <?php }} ?>
    </div>
    <div class="row">
        <div class="col-sm-3 col-md-2">
            <button id="reply_modal_button" type="button" class="btn btn-danger btn-sm btn-block" data-toggle="modal" data-target="#reply_modal">Reply</button>
            <hr />
        </div>
    </div> <!-- /container -->
    <?php include 'reply.php'; ?>
</div>
