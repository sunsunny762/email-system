<div class="modal fade" id="composer_modal" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" 
     data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Compose Email
                </h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">
                <form role="form" id="compose_email_form" method="post" action="#">
                    <?php if (is_array($users) && count($users) > 0) { ?>
                        <div class="form-group">
                            <label for="to" >To : </label>
                            <select class="modal_select form-control" name="c_to" id="c_to">
                                <option value="">Select User</option>
                                <?php foreach ($users as $value) { ?>
                                    <option value="<?= $value['user_id']; ?>"><?= $value['username']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="subject">Subject:</label>
                        <input type="text" class="form-control" id="c_subject" name="c_subject" placeholder="Enter Subject"/>
                        <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?php echo $user_id;?>" />
                    </div>
                    <div class="form-group">
                        <label for="message">Message:</label>
                        <textarea id="c_message" name="c_message" class="form-control" rows="3"></textarea>
                    </div>
                </form>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                    Close
                </button>
                <button type="button" class="btn btn-primary" onclick="compose_email();">
                    Send
                </button>
            </div>
        </div>
    </div>
</div>
<script>
    var base_url = '<?php echo base_url(); ?>';
    var user_id = '<?php echo $user_id; ?>';
    $("#compose_email_form").validate({
        rules: {
            c_subject: {
                required: true,
            },
            c_message: {
                required: true,
            },
        },
        messages: {
            c_subject: {
                required: "Please enter subject",
            },
            c_message: {
                required: "Please enter message",
            }
        },
        errorClass: "my-error-class"
    });

    function compose_email() {
        if ($("#compose_email_form").valid()) {
            var url = base_url+'inbox/compose_email';
            var data = $("#compose_email_form").serialize();
            var success = function (result)
            {
                if (result['result'] == 'success')
                {
                    showPopupNotification('SUCCESS', 'Message Sent Successfully');
                    $("#composer_modal").modal('hide');
                    setTimeout(function () {
                        reloadPagePart(base_url, '#inbox_container', 'inbox', 'index/'+user_id, {type: 'loadPart'});
                    }, 500);
                } else
                {
                    showPopupNotification('FAIL', 'Something went wrong !');
                }
            };
            insertDataQuery(url, data, success, 'json', null);
        }
    }
    
    function delete_email(id) {
        var res = confirm('Are you sure want to delete this email ?');
        if (!res) {
            return false;
        }
        var data = {id: id};
        var url = base_url + 'inbox/delete_email/' + id;
        var success = function (data) {
            if (data['result'] == 'success') {
                showPopupNotification('SUCCESS','Message Deleted Successfully!');
                setTimeout(function () {
                    reloadPagePart(base_url, '#inbox_container', 'inbox', 'index/'+user_id, {type: 'loadPart'});
                }, 500);
            } else {
                showPopupNotification('FAIL','Something Went Wrong!');
            }
        };
        insertDataQuery(url, data, success, 'json', null);
    }
</script>
