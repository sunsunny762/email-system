<?php header('Access-Control-Allow-Origin: *');?>
<div class="container" id="inbox_container">
    <div class="row">
        <h2 class="text-center">Email System</h2>
        <div class="col-sm-3 col-md-2">
            <button id="composer_modal_button" type="button" class="btn btn-danger btn-sm btn-block" data-toggle="modal" data-target="#composer_modal">COMPOSE</button>
            <hr />
            <ul class="nav nav-pills nav-stacked">
                <li class="active"><a href="<?php echo base_url('inbox/sent/'.$user_id);?>"> Inbox </a>
                </li>
                <li><a href="<?php echo base_url('inbox/sent/'.$user_id);?>">Sent Mail</a></li>
            </ul>
        </div>
        <div class="col-sm-9 col-md-10">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs">
                <li class="active"><a href="#home" data-toggle="tab"><span class="glyphicon glyphicon-inbox">
                        </span>Primary</a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane fade in active" id="home">
                    <div class="list-group">
                        <?php foreach ($data as $value) { ?>
                        <a href="<?php echo base_url('inbox/email/'.$value['c_id']);?>" class="list-group-item">
                                <span class="glyphicon glyphicon-star-empty"></span>
                                <span class="name" style="min-width: 120px;display: inline-block;"><?php echo $value['username']; ?></span> 
                                <span class=""><?php echo $value['subject']; ?></span>
                                <span class="text-muted" style="font-size: 11px;"><?php echo $value['message']; ?></span> 
                                <span class="badge"><?php echo date('d/m/Y H:i:s',strtotime($value['time']));?></span> 
                                <div class="pull-right">
                                    <button type="button"   onclick="delete_email('<?php echo $value['c_id']; ?>');return false;" class="btn btn-danger btn-sm btn-block"><i class="glyphicon glyphicon-trash"></i></button>
                                </div>
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include 'compose.php';?>
</div>

