<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
<link href="<?php echo base_url(); ?>assets/css/jquery.gritter.css" rel="stylesheet" />    
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.gritter.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/gritter.init.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/general.js"></script>
