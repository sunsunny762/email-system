<div class="modal fade" id="reply_modal" tabindex="-1" role="dialog" 
     aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" 
     data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" 
                        data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Reply Email
                </h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">
                <form role="form" id="reply_email_form" method="post" action="#">
                    <div class="form-group">
                        <label for="to" >To : </label>
                        <select class="modal_select form-control" name="r_to" id="r_to">
                           <option value="<?= $data['to']; ?>"><?= $data['username']; ?></option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="subject">Subject:</label>
                        <input type="text" class="form-control" id="r_subject" name="r_subject" placeholder="Enter Subject"/>
                        <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?php echo $user_id;?>" />
                        <input type="hidden" class="form-control" id="user_id" name="c_id" value="<?php echo $data['c_id'];?>" />
                    </div>
                    <div class="form-group">
                        <label for="message">Message:</label>
                        <textarea id="r_message" name="r_message" class="form-control" rows="3"></textarea>
                    </div>
                </form>
            </div>
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal">
                    Close
                </button>
                <button type="button" class="btn btn-primary" onclick="reply_email();">
                    Send
                </button>
            </div>
        </div>
    </div>
</div>
<script>
    var base_url = '<?php echo base_url(); ?>';
    var email_id = '<?php echo $email_id; ?>';
    $("#reply_email_form").validate({
        rules: {
            r_subject: {
                required: true,
            },
            r_message: {
                required: true,
            },
        },
        messages: {
            r_subject: {
                required: "Please enter subject",
            },
            r_message: {
                required: "Please enter message",
            }
        },
        errorClass: "my-error-class"
    });

    function reply_email() {
        if ($("#reply_email_form").valid()) {
            var url = base_url+'inbox/reply_email';
            var data = $("#reply_email_form").serialize();
            var success = function (result)
            {
                if (result['result'] == 'success')
                {
                    showPopupNotification('SUCCESS', 'Email Sent Successfully');
                    $("#reply_modal").modal('hide');
                    setTimeout(function () {
                        reloadPagePart(base_url, '#email_container', 'inbox', 'email/'+email_id, {type: 'loadPart'});
                    }, 500);
                } else
                {
                    showPopupNotification('FAIL', 'Something went wrong !');
                }
            };
            insertDataQuery(url, data, success, 'json', null);
        }
    }
</script>
